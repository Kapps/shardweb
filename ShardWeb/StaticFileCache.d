﻿module ShardWeb.StaticFileCache;
private import std.path;
private import ShardWeb.HttpRequestHandler;
private import std.stdio;
import std.datetime;
import std.conv;
import ShardWeb.HttpUtils;
import ShardTools.PathTools;
import ShardWeb.Site;
import std.exception;
import std.file;
import ShardTools.Compressor;
public import ShardTools.UnmanagedBuffer;
import core.stdc.string;

/// Manages the static files of a site, caching them in a compressed format upon demand.
/// Smaller files are cached in memory as well as on disk, whereas larger files are cached only on disk.
class StaticFileCache  {

// TODO: Instead of having static files contain raw data, make them a StaticPage class, or StaticFile.
// See plans.

public:
	/// Initializes a new instance of the StaticFileCache object.
	this(Site ParentSite) {
		this._ParentSite = ParentSite;		
/*			if(dirEntries(ParentSite.CacheDirectory, SpanMode.breadth).empty)				
				rmdir((ParentSite.CacheDirectory));
			else
				rmdirRecurse(ParentSite.CacheDirectory);*/

		// TODO: std.file bug above? Needs to delete dirs.
		if(!exists(ParentSite.CacheDirectory))
			mkdirRecurse(ParentSite.CacheDirectory);
		ClearCache(); // Clear remnant cache.
	}

	/// Gets the site using this file cache.
	@property Site ParentSite() {
		return _ParentSite;
	}

	/// Gets the cached file with the given location, or null if Uri is invalid.
	CachedFile GetFile(string Uri) {		
		Uri = HttpRequest.GetFixedURL(Uri);
		CachedFile* ResultPtr = (Uri in Files);
		if(ResultPtr)
			return *ResultPtr;		
		enforce(HttpUtils.ValidateURI(Uri), "Invalid URI \'" ~ Uri ~ "\'.");		
		synchronized {						
			CachedFile File = new CachedFile(Uri, this);
			Files[Uri] = File;
			return File;
		}
	}

	/// Clears the list of cached files, and deletes the files.
	void ClearCache() {
		foreach(string Key; Files.keys) {
			CachedFile Cached = Files[Key];
			string CompPath = Cached.GetPathToFile(true);			
			if(exists(CompPath))
				remove(CompPath);			
			Files.remove(Key);				
		}
	}

	~this() {
		ClearCache();
	}
	
private:
	CachedFile[string] Files;
	Site _ParentSite;	
}

/// Represents a single static file cached by the StaticFileCache.
class CachedFile {
public:
	this(string Uri, StaticFileCache Cache) {	
		this._Uri = Uri;
		while(_Uri.length > 0 && (_Uri[0] == '/' || _Uri[0] == '\\'))
			_Uri = _Uri[1..$];
		this._Cache = Cache;
		enforce(exists(GetPathToFile(false)), "The file to cache did not exist.");
		enforce(PathTools.IsPathWithinDirectory(Cache.ParentSite.RootDirectory ~ DirSeparator ~ Uri, Cache.ParentSite.RootDirectory), "Directory traversal exploit.");
		ulong Size = getSize(GetPathToFile(false));
		_StayInMemory = Size < 512 * 1024; // Don't cache things bigger than 512kb.
		if(_StayInMemory)
			writefln("Caching " ~ Uri ~ ".");
		else
			writefln("Not Caching " ~ Uri ~ ".");
		LastCheck = SysTime.min;
		LastModified = SysTime.min;
	}

	/// Gets the relative path to the file being cached.
	@property string Uri() {
		return _Uri;
	}

	/// Gets the cacher that owns this CachedFile.
	@property StaticFileCache Cache() {		
		return _Cache;
	}

	/// Gets the data contained by this file. 	
	/// This returns a reference to the actual data if cached in memory, and as such should never be edited, nor should it ever be stored for longer than the lifetime of this instance.
	/// Params:
	///		Encoding = The encoding to get the data as.
	immutable(void[]) Data(EncodingType Encoding) {	
		bool SkipCache = false;	
		Duration diff = Clock.currTime - LastCheck;		
		ulong minutes = cast(ulong)diff.total!"minutes";
		debug {
			minutes = ulong.max; // Don't cache in debug mode, we're developing.
		}
		if(minutes >= 1) { // Every minute, we check to see if it updated.
			LastCheck = Clock.currTime;
			SysTime NewMod = timeLastModified(GetPathToFile(false));
			if(NewMod != LastModified) {
				LastModified = NewMod;
				SkipCache = true;
				CachedCompressed = null;
				CachedUncompressed = null;
			}
		}
		void[] Data;
		bool NeedsCompressed = Encoding != EncodingType.Identity;
		if(!NeedsCompressed)
			Data = CachedUncompressed;
		else
			Data = CachedCompressed;
		if(Data is null) {
			synchronized {
				// In case something was waiting on the lock but completed the null check. We do this here to prevent having to monitor enter and exit on every single request.
				// Leads to code repetition, but may help performance.
				if(NeedsCompressed && CachedCompressed) {
					if(Encoding == EncodingType.Gzip)						
						return cast(immutable)CachedCompressed;
					else	
						return cast(immutable)Compressor.GzipToDeflate(cast(ubyte[])CachedCompressed);
				} else if(!NeedsCompressed && CachedUncompressed)
					return cast(immutable)CachedUncompressed;
				
				// TODO: Move to caching most popular items.
				string CachedPath = GetPathToFile(Encoding != EncodingType.Identity);
				bool NeedsWrite = false;
				bool NeedsCompress = false;
				void[] Result;
				if(!SkipCache && exists(CachedPath)) {
					Result = read(CachedPath);					
				} else {
					Result = read(GetPathToFile(false));
					NeedsCompress = true;
					NeedsWrite = true;
				}			
				if(NeedsCompress) {
					if(Encoding == EncodingType.Gzip || Encoding == EncodingType.Deflate)
						Result = Compressor.ToGzip(Result);
					else if(Encoding != EncodingType.Identity)
						throw new Exception("Unknown encoding type.");
				}
				if(NeedsWrite && Encoding != EncodingType.Identity) {
					if(!exists(PathTools.GetDirectoryPath(CachedPath)))
						mkdirRecurse(PathTools.GetDirectoryPath(CachedPath));
					std.file.write(CachedPath, Result);
				}
				if(StayInMemory) {
					if(Encoding == EncodingType.Identity)
						this.CachedUncompressed = Result;
					else
						this.CachedCompressed = Result;
				} else {
					if(Encoding == EncodingType.Deflate)
						return cast(immutable)Compressor.GzipToDeflate(Result);
					return cast(immutable)Result;
				}
			}
		}
		if(NeedsCompressed) {
			if(Encoding == EncodingType.Gzip)						
				return cast(immutable)CachedCompressed;
			else
				return cast(immutable)Compressor.GzipToDeflate(cast(ubyte[])CachedCompressed);
		} else
			return cast(immutable)CachedUncompressed;
	}	

	/// Deletes the data from memory, forcing a re-read from disk the next time it is used.
	void RemoveFromMemory() {
		CachedUncompressed = null;
		CachedCompressed = null;			
	}

	/// Gets or sets a value indicating whether this file should have it's contents remain in memory after a read.
	@property void StayInMemory(bool Value) {
		_StayInMemory = Value;
	}

	/// Ditto
	@property bool StayInMemory() const {
		return _StayInMemory;
	}

	package string GetPathToFile(bool Compressed) {
		string CacheUri;
		if(!Compressed)
			return _Cache.ParentSite.RootDirectory ~ _Uri;			
		else
			return _Cache.ParentSite.CacheDirectory ~ _Uri ~ ".gz";
	}

private:	
	bool _StayInMemory;
	void[] CachedUncompressed;
	void[] CachedCompressed;	
	string _Uri;
	StaticFileCache _Cache;
	/// The last time we did a check on last modified.
	SysTime LastCheck;
	/// The last modified date for this file.
	SysTime LastModified;
}