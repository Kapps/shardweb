﻿module ShardWeb.HttpUtils;
private import ShardNet.IResponse;
private import ShardWeb.StaticFileCache;
private import std.uri;
private import ShardTools.Logger;
private import ShardWeb.ErrorHandler;
import etc.c.zlib;
import ShardTools.PathTools;
import ShardNet.IRequest;
import std.zlib;
import std.string;
import ShardTools.ArrayOps;

/// Static helper used for common Http functions.
static class HttpUtils  {

public:	

	private static void GenerateContentTypes() {
		// Can't do this in static ctor due to dependencies.
		
		ContentTypes["json"] = "application/json";
		ContentTypes["js"] = "text/javascript"; // Technically application, but text is what's implemented for most browsers.
		ContentTypes["ogg"] = "application/ogg";
		ContentTypes["pdf"] = "application/pdf";
		ContentTypes["xhtml"] = "application/xhtml+xml";
		ContentTypes["zip"] = "application/zip";
		ContentTypes["gzip"] = "application/x-gzip";

		ContentTypes["gif"] = "image/gif";
		ContentTypes["jpeg"] = "image/jpeg";
		ContentTypes["jpg"] = "image/jpeg";
		ContentTypes["png"] = "image/png";
		ContentTypes["svg"] = "image/svg+xml";
		ContentTypes["tiff"] = "image/tiff";
		ContentTypes["tif"] = "image/tiff";
		ContentTypes["bmp"] = "image/bitmap";
		ContentTypes["ico"] = "image/vnd.microsoft.icon";

		ContentTypes["css"] = "text/css";
		ContentTypes["csv"] = "text/csv";
		ContentTypes["html"] = "text/html";
		ContentTypes["htm"] = "text/html";
		ContentTypes["txt"] = "text/plain";
		ContentTypes["xml"] = "text/xml"; // Same as js.

	}
	/// Gets the mime type for the given file, or null if it could not be determined.
	/// This list generally contains only mime types used for web sites.
	/// Params:
	///		FilePath = The path to the file. Only the extension is used, but it should still be a valid path.
	static string GetMimeType(string FilePath) {
		if(ContentTypes is null || ContentTypes.length == 0)
			GenerateContentTypes();
		string Extension = PathTools.GetExtension(FilePath);
		if(Extension.length > 0 && Extension[0] == '.')
			Extension = Extension[1..$];
		string* Result = Extension in ContentTypes;
		if(Result is null)
			return null;
		return *Result;	
	}

	/// Determines whether the specified content type is one that benefits from being compressed.
	/// Currently, this is text/* and application/javascript.
	/// Params:
	/// 	ContentType = The type of the content, such as "text/html".
	/// 	AdditionalSupported = Any additional content types that should be compressed. Case insensitive.
	public static bool ShouldCompressContentType(string ContentType, string[] AdditionalSupported = null) {
		if(ContentType is null)
			return false;			
		size_t IndexSemi = ContentType.IndexOf(';');
		if(IndexSemi != -1)
			ContentType = strip(ContentType[0..IndexSemi]);		
		ContentType = toLower(ContentType);
		size_t IndexSlash = ContentType.IndexOf('/');
		string BaseType;
		if(IndexSlash != -1)
			BaseType = strip(ContentType[0..IndexSlash]);
		if(BaseType == "text")
			return true;
		if(ContentType == "application/javascript")
			return true;
		foreach(string MimeType; AdditionalSupported) {
			if(icmp(MimeType, ContentType) == 0)
				return true;			
		}
		return false;
	}	

	unittest {
		string[] Additional = new string[3];
		Additional[0] = "image/jpg";
		Additional[1] = "text/css";
		Additional[2] = "text/html";
		assert(ShouldCompressContentType("  image / jpg", Additional));
		assert(ShouldCompressContentType("text/html; utf8", Additional));
		assert(ShouldCompressContentType("text/css", Additional));
		assert(!ShouldCompressContentType("image/png", Additional));
	}

	/// Determines the best encoding that the given request supports.
	/// Params:
	/// 	Request = The request to check the encoding supported for.
	public static EncodingType GetEncodingType(IRequest Request) {
		if(HttpUtils.SupportsEncoding(Request, "deflate"))
			return EncodingType.Deflate;
		if(HttpUtils.SupportsEncoding(Request, "gzip")) {
			
			// TODO: Temporary hack to disable gzip for Android. Still can't find out reason it doesn't like our format.
			// Just use etc.c.zlib.gz___ methods?
			string UserAgent = Request.Headers.Get!string("user-agent");
			string Forwarded = Request.Headers.Get!string("x-forwarded-for"); // Allow compression for things like Opera Mini.
			if(UserAgent.indexOf("Android") == -1 || Forwarded !is null)
				return EncodingType.Gzip;			
		}
		return EncodingType.Identity;
	}

	/// Returns whether the given Uri contains any invalid characters, allowing for some false positives to lower risk of undetected negatives.
	static bool ValidateURI(string Uri) {
		// TODO: Prevent needing decoding.
		try {
			Uri = decode(Uri);
			foreach(index, c; Uri) {
				if(c < 32 || c > 128)
					return false;
				if(c == '.' && ((index >= Uri.length - 1) || Uri[index + 1] == '.')) // Disallow .. and don't allow to end with .
					return false;
				// Much rather have false positives than not catching every exploit.
				if(c == '?' || c == '*' || c == ':' || c == '|' || c == '\"' || c == '<' || c == '>' || c == '%')
					return false;			
			}
			return true;
		} catch (Error e) {
			Log("errors", "Unable to validate URI \"" ~ Uri ~ "\".");
			return false;
		}
	} unittest {
		assert(ValidateURI("http://www.shardsoft.com"));
		assert(ValidateURI("http://www.shardsoft.com/test.d"));
		assert(!ValidateURI("http://www.shardsoft.com/../../etc/passwd"));
		assert(ValidateURI("http://www.shardsoft.com/.%2E/passwd"));
		assert(ValidateURI("http://www.shardsoft.com/%2E%2E/passwd"));
		assert(ValidateURI("http://www.shardsoft.com/%2E%00%2E/passwd"));
	}

	/// returns whether the given status code indicates success.	
	pure static bool IsSuccessfulStatusCode(uint StatusCode) {
		return StatusCode == 200 || StatusCode == 304 || StatusCode == 412;
	}

	/// Returns a simple error message from a status code, as defined in RFC2616.10, or a generic error message if an unknown status code.
	pure static string StatusCodeToPhrase(uint StatusCode) {
		switch(StatusCode) {
			case 100:
				return "Continue";
			case 101:
				return "Switching Protocols";		
			case 200:
				return "OK"; 			
			case 201:
				return "Created";
			case 202:
				return "Accepted";
			case 203:
				return "Non-Authorative Information";
			case 204:
				return "No Content";
			case 205:
				return "Reset Content";
			case 206:
				return "Partial Content";
			case 300:
				return "Multiple Choices";
			case 301:
				return "Moved Permanently";
			case 302:
				return "Moved Temporarily";
			case 303:
				return "See Other";
			case 304:
				return "Not Modified";
			case 305:
				return "Use Proxy";
			case 307:
				return "Temporary Redirect";
			case 400:
				return "Bad Request";
			case 401:
				return "Unauthorized";
			case 402:
				return "Payment Required";			
			case 403:
				return "Forbidden";
			case 404:
				return "Not Found";
			case 405:
				return "Method Not Allowed";
			case 406:
				return "Not Acceptable";
			case 407:
				return "Proxy Authentication Required";
			case 408:
				return "Request Timeout";
			case 409:
				return "Conflict";
			case 410:
				return "Gone";
			case 411:
				return "Length Required";
			case 412:
				return "Precondition Failed";
			case 413:
				return "Request Entity Too Large";
			case 414:
				return "Request-URI Too Long";
			case 415:
				return "Unsupported Media Type";
			case 416:
				return "Requested Range Not Satisfiable";
			case 417:
				return "Expectation Failed";
			case 500:
				return "Internal Server Error";
			case 501:
				return "Not Implemented";
			case 502:
				return "Bad Gateway";
			case 503:
				return "Service Unavailable";
			case 504:
				return "Gateway Timeout";
			case 505:
				return "HTTP Version Not Supported";
			default:
				break;
		}
		switch(StatusCode / 100) {
			case 1:
				return "Unknown Informational Status";
			case 2:
				return "Unknown Success Status";
			case 3:
				return "Unknown Redirection Status";
			case 4:
				return "Unknown Client Error";
			case 5:
				return "Unknown Server Error";
			default:
				return "Unknown Status Code";
		}
	}

	/// Checks if this request supports the given encoding.
	static bool SupportsEncoding(IRequest Request, string Encoding) {
		string Accepted = Request.Headers.Get!string("Accept-Encoding");
		string[] Split = split(Accepted, ",");
		foreach(string Value; Split)
			if(strip(Value) == Encoding)
				return true;
		return false;
	}

	private static __gshared string[string] ContentTypes;
}