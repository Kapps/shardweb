﻿module ShardWeb.CookieCollection;
private import ShardWeb.Cookie;
import ShardTools.StringTools;
import std.string;

/// Provides access to a collection of cookies.
/// Note that this is only a snapshot of the cookies as they were at the time.
struct CookieCollection  {

public:
	/// Initializes a new instance of the CookieCollection object.
	this(immutable(Cookie[]) Cookies) {
		this.Cookies = Cookies;
	}
	
	immutable(Cookie*) opIndex(in char[] Name) const {
		foreach(ref immutable Cookie cookie; Cookies)
			if(EqualsInsensitiveAscii(cookie.Name, Name))			
				return &cookie;
		return null;
	}

	int opApply(int delegate(Cookie) dg) {
		int Result = 0;
		foreach(c; Cookies)
			if((Result = dg(c)) != 0)
				break;
		return Result;		
	}

	/// Gets the number of cookies contained in the collection.
	@property size_t Count() const {
		return Cookies.length;
	}
	
private:
	immutable(Cookie[]) Cookies;
}