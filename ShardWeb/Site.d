﻿module ShardWeb.Site;
private import ShardTools.List;
public import ShardTools.ReadOnlyCollection;
private import std.array;
public import ShardWeb.ResponseCodeHandlerCollection;
public import ShardNet.BackendCollection;
import ShardNet.BackendConnection;
public import ShardWeb.StaticFileCache;
import std.file;
import std.exception;
import std.conv;
import std.algorithm;
import std.string; 
import ShardTools.PathTools;
import ShardTools.ArrayOps;
public import ShardWeb.RequestProcessor;
 
/// Represents a single website, with it's own host name and settings.
class Site  {

public:	

	// TODO: Consider a GetPage and GetAPI here.
	// The problem is if this isn't a backend site, that would not work well.
	// Take into consideration network backends or things like FastCGI backends.

	alias List!(RequestProcessor) RequestProcessorCollection;

	/// Initializes a new instance of the Site object.
	/// IMPORTANT:
	///		The directory passed in for CacheDirectory will usually be deleted upon program start and stop.
	this(string Name, string RootDirectory, string[] HostNames, string CacheDirectory) {
		this._RootDirectory = cast(immutable)PathTools.AddTrailingSlash(PathTools.MakeAbsolute(RootDirectory));
		this._HostNames = array(map!"toLower(a)"(HostNames));
		this._CacheDirectory = cast(immutable)PathTools.AddTrailingSlash(PathTools.MakeAbsolute(CacheDirectory));
		this._Name = Name;
		foreach(string HostName; HostNames) {
			enforce(startsWith(HostName, "http://"));
			//enforce(count!("a == '.'")(HostName) != 1, "A subdomain, such as images or www is required. Example: http://www.shardsoft.com instead of http://shardsoft.com - Host was " ~ HostName ~ ".");
		}
		enforce(exists(_RootDirectory), "The root directory must exist.");
		_Cache = new StaticFileCache(this);
		_Backends = new BackendCollection();
		_ResponseCodeHandlers = new ResponseCodeHandlerCollection(this);
		_Processors = new RequestProcessorCollection();
	}

	/// Gets the name of this site. This is a user-defined identifier used to distinguish between sites.
	@property string Name() const {
		return _Name;
	}

	/// Gets the root directory to use when locating files.
	@property string RootDirectory() const {
		return _RootDirectory;
	}

	/// Gets the directory to use for storing cached files, or null to disable caching.
	@property string CacheDirectory() const {
		return _CacheDirectory;
	}

	/// Returns the cache for static files used by this site.
	@property StaticFileCache Cache() {
		return _Cache;
	}

	/// Gets or sets any additional headers that should be sent for every page.
	@property ref string[string] ExtraHeaders() {
		return _ExtraHeaders;
	}

	/// Ditto
	@property void ExtraHeaders(string[string] Value) {
		this._ExtraHeaders = Value;
	}

	/// Gets the backends used for this Site.
	@property BackendCollection Backends() {
		return _Backends;
	}
	
	/// Gets the pages to use when a non-200 response code is returned.
	@property ResponseCodeHandlerCollection ResponseCodeHandlers() {
		return _ResponseCodeHandlers;
	}

	/// Gets all of the hostnames that can be used to connect to this site.
	/// Some valid hostnames are 'http://www.shardsoft.com' or 'http://images.shardsoft.com'.
	@property const(string[]) HostNames() const {
		return _HostNames;
	}

	/// Gets the request processors that apply to this site.
	@property RequestProcessorCollection Processors() {
		return _Processors;
	}

private:
	string _RootDirectory;	
	string _CacheDirectory;
	string _Name;
	string[string] _ExtraHeaders;	
	string[] _HostNames;
	StaticFileCache _Cache;	
	BackendCollection _Backends;
	ResponseCodeHandlerCollection _ResponseCodeHandlers;
	RequestProcessorCollection _Processors;
}