﻿module ShardWeb.SiteManager;
private import std.string;
private import ShardTools.ReadOnlyCollection;
import ShardTools.List;
import ShardWeb.Site;
import std.string : toLower, indexOf;
import std.algorithm : startsWith, count, remove;
import std.stdio : writefln;
import std.conv : to;
import ShardTools.ArrayOps;

/// Stores the currently available sites.
static class SiteManager  {

	alias ReadOnlyCollection!(Site,Site[]) SiteCollection;
	alias Event!(void, Site) SiteEvent;

public:
	/// Initializes a new instance of the SiteManager object.
	shared static this() {
		_SiteAdded = new SiteEvent();
		_SiteRemoved = new SiteEvent();
	}

	/// Adds the given site to the collection.
	static void AddSite(Site site) {		
		foreach(Site Other; Sites)
			if(icmp(Other.Name, site.Name) == 0)
				throw new Exception("Cannot have multiple sites with the same name.");
		foreach(Site Other; Sites) {
			foreach(string OtherHost; Other.HostNames)
				if(site.HostNames.Contains(OtherHost))
					throw new Exception("Can not have multiple sites with the same host name.");
		}		
		Sites ~= site;
		SiteAdded.Execute(site);		
	}

	/// Gets the site with the given host name.
	static Site GetSite(string HostName) {	
		HostName = GetFixedHostName(HostName);	
		foreach(Site site; Sites) {
			foreach(string SiteHostName; site.HostNames) {
				if(HostName == SiteHostName)
					return site;
			}
		}		
		return null;
	}	

	/// Provides a readonly view of all the sites the SiteManager contains.
	@property static SiteCollection AllSites() {
		return SiteCollection(Sites);
	}

	/// Removes the given site from the SiteManager.
	static bool RemoveSite(Site site) {
		size_t Index = Sites.IndexOf(site);
		if(Index == -1)
			return false;
		Sites.remove(Index);
		SiteRemoved.Execute(site);
		return true;
	}

	/// Returns the given host name in a format used by each Site.
	static inout(char[]) GetFixedHostName(inout char[] HostName) {	
		char[] Copy = cast(char[])HostName.dup;				
		//size_t NumDots = Count!"a == '.'"(Copy);
		Copy = toLower(Copy);
		if(indexOf(Copy, "http://") == -1)
			Copy = "http://" ~ Copy;		
		//if(NumDots == 1) // If no subdomain, append www. automatically. This is now removed because you can specify multiple hostnames.
			//Copy = Copy[0.."http://".length] ~ "www." ~ Copy["http://".length..$];		
		return cast(inout)Copy;
	}

	/// An event for when a site is added or removed.
	static @property SiteEvent SiteAdded() {
		return _SiteAdded;
	}

	/// Ditto
	static @property SiteEvent SiteRemoved() {
		return _SiteRemoved;
	}
	
	private static __gshared Site[] Sites;
	private static __gshared SiteEvent _SiteAdded;
	private static __gshared SiteEvent _SiteRemoved;
}