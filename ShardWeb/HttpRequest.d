﻿module ShardWeb.HttpRequest;
private import core.atomic;
private import std.exception;
private import ShardTools.PathTools;
private import ShardTools.ExceptionTools;
public import ShardWeb.CookieCollection;
private import ShardTools.StringTools;
public import ShardWeb.Cookie;
import std.conv;
import ShardTools.Map;
import ShardNet.IRequest;
import std.algorithm;
import std.string;
import std.socket;
import ShardTools.ArrayOps;

mixin(MakeException("HttpException"));

enum HttpMethod {
	Unknown = 0,
	Get = 1,
	Post = 2
}
 
/// Represents a single incoming HTTP request.
class HttpRequest : IRequest {

public:	

	/// Initializes a new instance of the HttpRequest object.
	/// Params:
	/// 	RequestedPage = The relative path to the page being requested.
	/// 	PageParameters = Any query parameters for this request.
	/// 	ProtocolParameters = Any protocol parameters for this request.
	///		RemoteAddress = The address requesting this page.
	/// 	Cookies = The cookies that this request has.
	/// 	Type = The type of page being requested.
	this(string RequestedPage, StringMap PageParameters, StringMap ProtocolParameters, Address RemoteAddress, Cookie[] Cookies, RequestType Type) {
		this.RequestUri = GetFixedURL(RequestedPage);
		this._PageParameters = PageParameters;
		this._ProtocolParameters = ProtocolParameters;
		this._RemoteAddress = RemoteAddress;
		this._RequestID = atomicOp!("+=")(NextID, 1);
		this._Type = Type;		
		//PageParameters.MakeReadonly();
		//ProtocolParameters.MakeReadonly();
		this._Cookies = CookieCollection(Cookies.idup);

		string Method = Headers.Get("Method", "Unknown");
		if(EqualsInsensitiveAscii(Method, "Get"))
			this._Method = HttpMethod.Get;
		else if(EqualsInsensitiveAscii(Method, "Post"))
			this._Method = HttpMethod.Post;
		else
			this._Method = HttpMethod.Unknown;
	}
	
	/// Gets the cookies sent by the request.
	@property CookieCollection Cookies() const {
		return _Cookies;
	}	

	/// Gets the page that this request requested.
	@property string UriRequested() const {
		return RequestUri;
	}

	/// Gets the address requesting this page.
	@property Address RemoteAddress() {
		return _RemoteAddress;
	}

	/// Gets the parameters for the page query.
	@property StringMap Parameters() {
		return _PageParameters;
	}

	/// Gets the parameters / headers for the HTTP protocol.
	@property StringMap Headers() {
		return _ProtocolParameters;
	}

	/// Gets a unique identifier representing this request. Generally, this is just an auto-increment field.
	@property ulong RequestID() const {
		return _RequestID;
	}

	/// Gets the type of page being requested.
	@property RequestType Type() const {
		return _Type;
	}

	/// Returns whether this request came from a mobile device, such as a phone or tablet.	
	@disable @property bool IsMobile() const {
		return false; // TODO.
	}

	/// Gets the http method used to make this request, such as Get or Post.
	@property HttpMethod Method() const {
		return _Method;
	}

	/// Terminates this request, indicating that a response is no longer needed.
	void Terminate() {
		_IsTerminated = true;
	}

	/// Indicates whether this request was terminated.
	/// An example of this is the remote host disconnected prior to a response being fully generated.
	@property bool IsTerminated() const {
		return _IsTerminated;
	}

	/// Performs a server-side redirect of this request to the given URI.
	/// This must be called prior to a response being created.
	/// Params:
	/// 	NewURI = The new URI to be redirected to, relative to the site root directory.
	void RewriteUrl(string NewURI) {
		//assert(IsValidURI(NewURI));
		this.RequestUri = GetFixedURL(NewURI);
	}

	/// Returns the fixed version of the URL, in the format that the Dispatcher prefers.
	public static string GetFixedURL(string URL) {		
		if(URL.length == 0)
			return "";
		enforce(!PathTools.IsAbsolute(URL) || URL[0] == '/' || URL[0] == '\\', "The path to a URL must be relative.");
		while(URL.length > 0 && (URL[0] == '\\' || URL[0] == '/'))
			URL = URL[1..$];
		if(URL.length == 0)
			URL = '/' ~ URL;
		if(URL[0] != '/')
			URL = '/' ~ URL;
		if(URL[URL.length - 1] == '\\' || URL[URL.length - 1] == '/')
			URL ~= "index.d";
		if(!URL.Contains('.'))
			URL ~= ".d";
		return URL;
	}

private:
	CookieCollection _Cookies;
	Address _RemoteAddress;
	string RequestUri;	
	StringMap _PageParameters;
	StringMap _ProtocolParameters;		
	ulong _RequestID;	
	RequestType _Type;
	bool _IsTerminated = false;
	HttpMethod _Method;
	static shared ulong NextID = 1;		
}