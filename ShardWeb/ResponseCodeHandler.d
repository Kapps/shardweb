﻿module ShardWeb.ResponseCodeHandler;
private import std.variant;
private import ShardWeb.HttpRequest;
private import ShardWeb.HttpResponse;
private import ShardWeb.Site;

import ShardWeb.Page;
import std.conv;

/// The base class for a page to handle the response of a non-200 status code, providing additional information about the request in the ProtocolParameters parameter.
/// Not every non-200 response is handled, as the server will handle anything that occurs when trying to dispatch the request (such as a 301).
/// A ResponseCodeHandler operates locally. Requests are not dispatched to a backend, but rather handled by the RequestHandler and dispatched to the containing Site.
abstract class ResponseCodeHandler {

public:
	/// Initializes a new instance of the ResponseCodeHandler object.
	this(Site SiteContaining) {
		this._SiteContaining = SiteContaining;
	}	
 	
	/// Gets the site that this handler applies for.
	@property Site SiteContaining() {
		return _SiteContaining;
	}

	/// Handles a Response that did not return a status code of 200.
	/// Params:
	/// 	Request = The original request, forwarded as is.
	/// 	ExistingResponse = The result of attempting to handle the request; can be null.
	/// 	AdditionalInfo = Any additional information about the request. Guaranteed to have string:uint StatusCode, string:string URI (can be null), and string:Site Site (can be null).
	abstract HttpResponse HandleUnsuccessfulResponse(HttpRequest Request, HttpResponse ExistingResponse, Variant[string] AdditionalInfo);
	
private:
	Site _SiteContaining;
}