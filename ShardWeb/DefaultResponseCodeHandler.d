﻿module ShardWeb.DefaultResponseCodeHandler;
private import std.variant;
private import std.exception;
private import ShardWeb.Site;
private import ShardWeb.HttpUtils;
private import std.conv;
public import ShardWeb.HttpResponse;
public import ShardWeb.HttpRequest;
public import ShardWeb.ResponseCodeHandler;


/// The default handler for any otherwise unhandled response code.
/// A singleton instance is available for when a Site is not available.
class DefaultResponseCodeHandler : ResponseCodeHandler {

public:
	/// Initializes a new instance of the DefaultResponseCodeHandler object.
	this(Site SiteContaining) {
		super(SiteContaining);
	}

	/// Handles a Response that did not return a status code of 200.
	/// Params:
	///		Request = The original request, forwarded as is.
	///		ExistingResponse = The result of attempting to handle the request; can be null.
	///		AdditionalInfo = Any additional information about the request. Guaranteed to have string:uint StatusCode, string:string URI (can be null), and string:Site Site (can be null).
	override HttpResponse HandleUnsuccessfulResponse(HttpRequest Request, HttpResponse ExistingResponse, Variant[string] AdditionalInfo) {
		HttpResponse Response = new HttpResponse(Request);
		uint StatusCode = AdditionalInfo["StatusCode"].get!uint;
		string StatusCodeString = to!string(StatusCode);
		Response.AppendBody("<html><head><title>Error " ~ StatusCodeString ~ "</title></head>");		
		Response.AppendBody("<body><h1>Error " ~ StatusCodeString ~ " - " ~ HttpUtils.StatusCodeToPhrase(StatusCode) ~ " </h1>");
		Response.AppendBody("<p>An error has occurred when submitting your request. If this happens frequently, please contact the administrator.</p>");
		Response.AppendBody("<p>If you wish to go back to the previous page, click <a href=\"javascript:history.go(-1)\">here</a> to return to the previous page, "
			~ "otherwise, click <a href=\"/\">here</a> to return to the home page.</p></body></html>");
		return Response;
	}

	/// Gets or sets a global instance of the DefaultResponseHandler, with no containing site.
	@property static DefaultResponseCodeHandler Global() {
		if(_Global is null)
			_Global = new DefaultResponseCodeHandler(null);		
		return _Global;
	}

	/// Ditto
	@property static void Global(DefaultResponseCodeHandler Value) {
		enforce(Value.SiteContaining is null, "Global response handler must not be part of a site.");
		_Global = Value;
	}
	
private:
	static __gshared DefaultResponseCodeHandler _Global;
}