﻿module ShardWeb.HttpOutput;

import ShardIO.OutputSource;

/+
/// An OutputSource that parses the InputSource for http headers, and creates HttpRequests to pass along whenever one is received.
class HttpOutput : OutputSource {

public:
	/// Initializes a new instance of the HttpOutput object.
	this() {
		
	}
	
private:
	Event!HttpRequest RequestReceived;
}
+/