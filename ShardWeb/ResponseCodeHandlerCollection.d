﻿module ShardWeb.ResponseCodeHandlerCollection;
private import std.exception;
private import ShardWeb.DefaultResponseCodeHandler;
private import ShardWeb.Site;
private import ShardWeb.ResponseCodeHandler;


/// A collection consisting of zero or more ResponseCodeHandlers.
class ResponseCodeHandlerCollection  {

public:
	/// Initializes a new instance of the ResponseCodeHandlerCollection object.	
	/// Params:
	/// 	SiteContaining = The site containing this collection.
	this(Site SiteContaining) {
		_Default = new DefaultResponseCodeHandler(SiteContaining);
		this._SiteContaining = SiteContaining;
	}

	/// Gets the handler for the given response code. The default handler is returned if the ResponseCode is not registered.
	/// Params:
	/// 	ResponseCode = The response code to get the handler for.
	@property ResponseCodeHandler opIndex(uint ResponseCode) {
		ResponseCodeHandler* Result = (ResponseCode in _Handlers);
		if(Result is null)
			Result = &_Default;
		return *Result;
	}
	
	/// Gets or sets the default ResponseCodeHandler, used when no other handlers match.
	@property ResponseCodeHandler Default() {		
		return _Default;
	}	

	/// Ditto
	@property void Default(ResponseCodeHandler Value) {
		enforce(Value !is null, "Must have a default response code handler; can not be null.");
		this._Default = Value;
	}
	
private:
	ResponseCodeHandler _Default;
	ResponseCodeHandler[uint] _Handlers;
	Site _SiteContaining;
}