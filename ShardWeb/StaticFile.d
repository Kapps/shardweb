﻿module ShardWeb.StaticFile;
public import ShardWeb.Site;

import ShardWeb.StaticPage;

/// Provides a file-system based static page, optionally transformed.
@disable class StaticFile : StaticPage {

// TODO: Implement this. The basic idea is pretty simple:
// 1) Upon startup, scan through all directories and create a StaticFile for each... static file.
//		Make sure it doesn't conflict with any dynamic pages.
// 2) Upon request, retrieve contents of static file and transform it.
//		If cached, just go ahead and serve it.
//		If not cached, Yield until part of disk read completes, transform and send that part, continue.
// 3) Send response, probably a chunk at a time unless small and/or cached.
//		Make sure AppendBody just overwrites buffer, not actually performs a copy.

public:
	/// Initializes a new instance of the StaticFile object.
	this(Site SiteContaining, string Uri) {
		super(SiteContaining, Uri, CacheStyle.Memory | CacheStyle.Disk);
	}
	
private:
}