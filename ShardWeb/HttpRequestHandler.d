﻿module ShardWeb.HttpRequestHandler;
private import std.typecons;
private import std.path;
private import ShardTools.LinkedList;
private import ShardTools.Queue;
private import ShardTools.NativeReference;
private import std.variant;
private import ShardWeb.DefaultResponseCodeHandler;
private import ShardWeb.ResponseCodeHandler;
private import ShardTools.TimeSpan;
private import ShardTools.Compressor;
private import ShardWeb.StaticFileCache;
private import ShardTools.DateParse;
private import std.datetime;
private import std.file;
private import ShardWeb.HttpUtils;
private import std.parallelism;
private import std.c.stdlib;
import ShardTools.Logger;
import ShardWeb.SiteManager;
import ShardWeb.Site;
import ShardTools.PathTools;
import ShardWeb.ErrorHandler;
import std.conv;
import ShardWeb.Page;
import std.algorithm;
import ShardWeb.HttpResponse;
import ShardNet.IRequest;
import ShardNet.IRequestHandler;
import core.thread;
import ShardTools.ArrayOps;

import ShardTools.List;
import ShardTools.Map;
import std.exception;
import std.stdio;
 
public import ShardWeb.PageMapping;
public import ShardWeb.HttpRequest;
public import ShardNet.IRequestHandler;

//version=HighCPU;

/// A request handler for HttpRequests by creating a mapping of a Url to a Page.
/// Performance:
///		This class gets slightly better performance when version HighCPU is set, but it uses ~60% more cpu usage on the test machine.
///		Using the standard low CPU usage then can result in higher overall performance by using more backend processes or round robin DNS with multiple servers.
///		As an example, low CPU gets ~19000 hello world requests on the test machine, high cpu gets ~20500, and four copies of low CPU results in a total of ~45000.
final class HttpRequestHandler : IRequestHandler  {

public:
	
	// TODO: Refactor this. At least separate dynamic requests from static requests.
	// Or, even better, merge the two, and thus allow support for StaticPageTransformers.
	// This also allows interesting things with yielding while waiting for the file, without special casing it.
	// The issue is whether or not it supports large files well.

	/// Initializes a new instance of the Dispatcher object.
	/// Params:
	///		NumWorkers = The number of worker threads used to process page requests.
	this(size_t NumWorkers = 10) {		
		this.NumWorkers = NumWorkers;
		PageMap = new Map!(string, PageMapping);		
		ThreadIDs = new Map!(core.thread.Thread, size_t)();		
		version(HighCPU)
			this._Pool = new TaskPool(NumWorkers);
	}	
	 
	/// Queues a request to be processed.
	/// Params:
	/// 	Request = The request to process and generate a response for.
	/// 	ResponseHandler = A callback to invoke when a response is generated for the request.
	/+final+/ void QueueRequest(IRequest Request, void delegate(IResponse) ResponseHandler) {			
		version(HighCPU) {
			_Pool.put(task(&HandleRequest, Request, ResponseHandler));			
		} else {
			HandleRequest(Request, ResponseHandler);			
		}
	}

	private void HandleRequest(IRequest Request, void delegate(IResponse) ResponseHandler) {			
		IResponse Response = ProcessRequest(Request);		
		if(Response)
			ResponseHandler(Response);		
	}

	/// Dispatches a single request to the appropriate PageMapping on this thread.
	/// Params:
	/// 	Request = The request to dispatch.	
	/+final+/ HttpResponse ProcessRequest(IRequest Request) {					
		HttpRequest Casted = cast(HttpRequest)Request;
		HttpResponse Response = new HttpResponse(Casted);
		if(Casted is null) {			
			Response.StatusCode = 500;
			return Response;
		}
		if(Casted.IsTerminated)
			return null;		
		Site SiteContaining = null;
		try {
			Response.AddHeader("Content-Type", "text/html; charset=UTF-8"); // Default to plain text.
			Response.AddHeader("Cache-Control", "no-cache"); // Make sure we don't cache things we shouldn't.			
			if(Casted.UriRequested is null) {
				Response = GetUnsuccessfulResponseResult(500, Casted.UriRequested, Casted, Response, SiteContaining, null);				
				return Response;
			} 
			assert(HttpUtils.ValidateURI(Casted.UriRequested)); // Server does this but make sure anyways.			
			string HostName = Request.Headers.Get!string("host");
			if(!HostName.startsWith("http://")) // TODO: https-only site?
				HostName = "http://" ~ HostName;
			SiteContaining = SiteManager.GetSite(HostName);
			if(SiteContaining is null) {
				Response = GetUnsuccessfulResponseResult(404, Casted.UriRequested, Casted, Response, SiteContaining, null);
				return Response;
			}			
			StateCollection States = Preprocess(Casted, SiteContaining);			
			HttpResponse Handled = HandleResponse(Casted, Response, SiteContaining);						
			Postprocess(Casted, Handled, States);
			return Handled;
		} catch(Throwable Ex) {			
			debug writeln("Error in ProcessRequest: ", Ex);
			ErrorHandler.NotifyError(Casted, Ex.msg ~ " in " ~ Ex.file ~ " on line " ~ to!string(Ex.line) ~ ".");
			Variant[string] Additional;
			Additional["Exception"] = Ex;
			Response = GetUnsuccessfulResponseResult(500, Casted.UriRequested, Casted, Response, SiteContaining, Additional);
			return Response;
		}
	}

	private struct StateData {
		RequestProcessor Processor;
		void* State;
	}
	alias LinkedList!StateData StateCollection;

	private StateCollection Preprocess(HttpRequest Request, Site SiteContaining) {		
		StateCollection States = new StateCollection();		
		foreach(RequestProcessor Proc; SiteContaining.Processors) {
			if(Proc.Pattern.Match(Request.UriRequested)) {
				StateData data;
				data.Processor = Proc;
				data.State = Proc.PreprocessRequest(Request);
				States.Add(data);
			}
		}
		return States;
	}

	private void Postprocess(HttpRequest Request, HttpResponse Response, StateCollection States) {
		foreach(ref StateData State; States)
			State.Processor.PostprocessRequest(Request, Response, State.State);		
	}
	
	private HttpResponse GetUnsuccessfulResponseResult(uint ResponseCode, string URI, HttpRequest Request, HttpResponse ExistingResponse, Site SiteContaining, Variant[string] AdditionalInfo) {
		ResponseCodeHandler ResponseHandler;
		if(SiteContaining is null)
			ResponseHandler = DefaultResponseCodeHandler.Global;
		else
			ResponseHandler = SiteContaining.ResponseCodeHandlers[ResponseCode];
		AdditionalInfo["StatusCode"] = ResponseCode;		
		AdditionalInfo["URI"] = URI;
		AdditionalInfo["Site"] = SiteContaining;
		HttpResponse Response = ResponseHandler.HandleUnsuccessfulResponse(Request, ExistingResponse, AdditionalInfo);
		Response.AddHeader("Content-Type", "text/html; charset=UTF-8");
		Response.AddHeader("Cache-Control", "no-cache"); // Never cache errors.		
		return Response;
	}

	protected HttpResponse HandleResponse(HttpRequest Request, HttpResponse Response, Site site) {
		foreach(Key, Value; site.ExtraHeaders)
			Response.AddHeader(cast(char[])Key, cast(char[])Value);
		string Uri = Request.UriRequested;
		if(Request.Type == RequestType.Dynamic) {			
			if(site.HostNames.length == 0)
				return GetUnsuccessfulResponseResult(500, Uri, Request, Response, site, null);											
			string FullUri = site.HostNames[0] ~ Uri; // This is safe because HostNames is immutable and we add each of the site's hostnames as a page-mapping.
			PageMapping Mapping = PageMap.Get(FullUri, null);					
			if(Mapping is null)
				return GetUnsuccessfulResponseResult(404, Uri, Request, Response, site, null);								
			Page Responder = Mapping.GetPageInstanceForThread(ThreadIDs.Get(core.thread.Thread.getThis(), 0));			
			Responder.HandleRequest(Request, Response);			
			string Body = cast(string)Response.Content;
			bool ShouldCompress = Response.ContentEncoding == EncodingType.Identity && Body.length > 512;						
			if(ShouldCompress) {
				Response.ContentEncoding = HttpUtils.GetEncodingType(Response.Request);
				if(Response.ContentEncoding == EncodingType.Deflate)
					Body = cast(string)Compressor.ToDeflate(cast(void[])Body);
				else if(Response.ContentEncoding == EncodingType.Gzip)
					Body = cast(string)Compressor.ToGzip(cast(void[])Body);							
			}		
			Response.SetBody(Body);
			return Response;			
		} else {
			string FilePath = PathTools.MakeAbsolute(site.RootDirectory ~ DirSeparator ~ Uri);
			if(!PathTools.IsPathWithinDirectory(FilePath, site.RootDirectory))
				return GetUnsuccessfulResponseResult(403, Uri, Request, Response, site, null);
			if(!exists(FilePath) || PathTools.IsHidden(FilePath))
				return GetUnsuccessfulResponseResult(404, Uri, Request, Response, site, null);
			string MimeType = HttpUtils.GetMimeType(FilePath);			
			DateTime LastModified = cast(DateTime)timeLastModified(FilePath).toUTC();
			DateTime Expires = cast(DateTime)(Clock.currTime() + dur!"days"(7));			
			string LastModString = DateParse.toHttp(LastModified);
			string UserLastModified = Response.Request.Headers.Get!string("If-Modified-Since", DateParse.toHttp(DateTime(1970, 01, 01, 0, 0, 0)));
			string ExpiresString = DateParse.toHttp(Expires);
			string UserLastUnmodString = Response.Request.Headers.Get!string("If-Unmodified-Since", null);
			DateTime UserLastModifiedTime = DateParse.parseHttp(UserLastModified);				
			DateTime UserLastUnmodTime = UserLastUnmodString is null ? DateTime.max : DateParse.parseHttp(UserLastUnmodString, DateTime.max);
			if(UserLastUnmodString !is null && UserLastUnmodTime > LastModified) {
				Response.StatusCode = 412;
				return Response;
			}
			Response.AddHeader("Last-Modified", LastModString);
			Response.AddHeader("Expires", ExpiresString);			
			void[] Output;
			EncodingType OutputEncoding = EncodingType.Identity;
			if(UserLastModifiedTime < LastModified) {							
				OutputEncoding = HttpUtils.GetEncodingType(Response.Request);
				if(!HttpUtils.ShouldCompressContentType(MimeType))
					OutputEncoding = EncodingType.Identity;				
				Output = cast(void[])site.Cache.GetFile(Uri).Data(OutputEncoding);									
				Response.StatusCode = 200;
				Response.AppendBody(cast(char[])Output);
				Response.ContentEncoding = OutputEncoding;
				Response.AddHeader("Content-Type", MimeType); // Overwrite plain text with the actual encoding.
				Response.AddHeader("Cache-Control", "max-age:" ~ to!string((cast(SysTime)Expires - Clock.currTime()).total!"seconds"));				
			} else { 				
				Response.StatusCode = 304;
			}
			return Response;
		}
	}

	/// Creates or overwrites a mapping from the given URL to the given handler.
	/// Params:
	/// 	Path = The page to the url, such as /index.d.
	/// 	Mapping = The PageMapping to respond to requests to the given Path with. Can be null to remove the PageMap.
	private void SetPageMap(string Path, PageMapping Mapping) {		
		// TODO: Private until set a better way of creating one instance per thread.		
		string Fixed = HttpRequest.GetFixedURL(Path);		
		bool DisplayedMessage = false;
		foreach(string HostName; Mapping.SiteContaining.HostNames) {
			string Mapped = HostName ~ Fixed; // Make it per-site.
			if(Mapping is null) {	
				if(!DisplayedMessage) {
					writefln("%s", "Removing mapping for page at \'" ~ Fixed ~ "\' on " ~ Mapping.SiteContaining.Name ~ ".");
					DisplayedMessage = true;
				}
				PageMap.Remove(Path);
			} else {
				if(!DisplayedMessage) {
					writefln("%s", "Assigned \'" ~ Fixed ~ "\' on " ~ Mapping.SiteContaining.Name ~ " to \'" ~ typeid(Mapping.GetPageInstanceForThread(0)).name ~ "\'.");		
					DisplayedMessage = true;
				}
				PageMap.Set(Mapped, Mapping);
			}
		}
	}
	
	/// Goes through all of the modules in the current program and finds all types derived from Page that are not abstract.
	/// An instance of each type is created for each worker thread, and a PageMapping is assigned for it.
	/// Params:
	/// 	Pattern = The pattern that the fully qualified class deriving from Page must match in order to be added. For example, '^ShardSite\..+' would match ShardSite.*, or anything from the ShardSite package.
	void AddPagesFromModules(Regex!char Pattern) {
		foreach(mod; ModuleInfo) {
			foreach(TypeInfo_Class ti; mod.localClasses) {				
				try {
					for(TypeInfo_Class Base = ti.base; Base !is null; Base = Base.base) {
						if(Base.name == "ShardWeb.Page.Page") {																					
							if(IsAbstract(ti) || match(ti.name, Pattern).empty())
								continue;
							Page Instance = cast(Page)Object.factory(ti.name);											
							string Url = HttpRequest.GetFixedURL(Instance.Url());							
							PageMapping Map = new PageMapping(Url, () { return cast(Page)Object.factory(ti.name); }, Instance.SiteContaining);
							SetPageMap(Url, Map);							
						} else if(Base.name == "ShardWeb.RequestProcessor.RequestProcessor") {
							if(IsAbstract(ti) || match(ti.name, Pattern).empty())
								continue;
							RequestProcessor Instance = cast(RequestProcessor)Object.factory(ti.name);
							Site Containing = Instance.SiteContaining;
							Containing.Processors.Add(Instance);
							writeln("Added request processor of type " ~ ti.name ~ " to " ~ Containing.Name ~ ".");							
						}
					}
				} catch (Throwable Ex) {
					string Details = "Unable to register page " ~ ti.name ~ ". Ensure it may compile without errors (DMD may allow errors in classes that are unused without reflection, and disabled classes may be used), and that the constructor does not throw. Details:\r\n" ~ to!string(Ex);
					writefln(Details);
					Logger.Default.LogMessage("errors", Details);					
				}
			}
		} 
	}

	private bool IsAbstract(TypeInfo_Class ti) {
		bool IsAbstract = false;
		// TODO: Check if DMD >= 2.056 and just use Object.factory and check null.
		foreach(void* Pointer; ti.vtbl) {
			if(Pointer is null) {
				IsAbstract = true;
				break;
			}
		}
		if(IsAbstract)
			writefln("Skipping " ~ ti.name ~ " as it appears to be abstract.");		
		return IsAbstract;			
	}

private:			

	Map!(string, PageMapping) PageMap;
	Map!(core.thread.Thread, size_t) ThreadIDs;	
	size_t NumWorkers;	
	version(HighCPU) TaskPool _Pool;	
}