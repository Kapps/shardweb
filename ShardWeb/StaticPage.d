﻿module ShardWeb.StaticPage;
private import ShardIO.MemoryOutput;
private import core.sync.mutex;
private import std.array;
private import ShardIO.FileInput;
private import ShardIO.IOAction;
private import std.file;
private import core.time;
private import ShardWeb.Page;
import ShardTools.Untyped;

// TODO: Consider allowing a Network cache. The reasoning is that multiple backends should not be forced to keep the same thing cached.
enum CacheStyle {
	None = 0,
	Memory = 1,
	Disk = 2
}

/// Represents a page that is generated once then sent repeatedly.
/// Thus, each static page can be cached.
/// It is important to note that due to the distributed nature of the web, static pages should not depend on state to be generated.
/// As a result, the generated page does not have an HttpRequest passed in.
/// This also allows easier access for a server-side API if desired.
class StaticPage : Page {

public:

	// TODO: This essentially works as follows:
	// 1) On startup, a StaticPage is created for all files in the root directory and subfolders.
	//		-A watcher is used to scan for changes.
	// 2) Upon request, the page has GeneratePage called.
	//		-If CacheType is not None, the page is cached (ideally in memory).
	// 3) As more pages get cached, some things must move away from memory.
	//		-When a page is about to be moved away from memory, check if Disk is set, and if so cache it to disk.
	// 4) Send the cached page.
	// PROBLEM: Backends prevent just using asynchronous IO in a good way.
	// SOLUTION: Use Yield while waiting for a page to be cached. Make sure GeneratePage can yield then.
	//			 Also, make sure that reading from disk CacheType uses async IO as well.	

	/// Initializes a new instance of the StaticPage object.
	this(Site SiteContaining, string Uri, CacheStyle CacheType = CacheStyle.Disk | CacheStyle.Memory) {
		super(Uri, SiteContaining);
		_CacheDuration = dur!"weeks"(4);
		this._CacheType = CacheType;
	}

	/// Indicates the locations that this page can be cached to.
	/// For example, reading from a file should be cached in memory if small enough.
	/// If the file is set to be compressed, that will be cached as well.
	@property final CacheStyle CacheType() const {
		return _CacheType;
	}
 
	/// Clears the cached data from this page, causing it to be generated upon the next request.
	void MarkDirty() {
		synchronized(this) {
			_CachedContents = null;
		}
	}

	/// Gets or sets the amount of time this page will be cached for by the client (NOT server-side, as it is cached until marked dirty).
	@property Duration CacheDuration() const {
		return _CacheDuration;
	}

	/// Ditto
	@property void CacheDuration(Duration Value) {
		_CacheDuration = Value;
	}

	override void HandleRequest(HttpRequest Request, HttpResponse Response) {
		synchronized(this) {
			if(_CachedContents is null) {
				_CachedContents = GeneratePage();
			}
		}	
		Response.AppendBody(_CachedContents);
	}

protected:

	/// Generates the response to be sent for this page.
	abstract string GeneratePage();

	/// Removes the cached contents from memory.
	/// If CacheType allows for caching in locations besides memory, it will be done here.
	void Unload() {
		synchronized(this) {
			if(_CacheType & CacheStyle.Disk) {
				WritePending = true;
			}
		}
	}

	void Reload() {
		synchronized(this) {		
			if(_CacheType & CacheStyle.Disk) {
				string DiskPath = PathForCachedFile();
				if(exists(DiskPath)) {					
					FileInput Input = new FileInput(DiskPath);
					MemoryOutput Output = new MemoryOutput();
					IOAction Action = new IOAction(Input, Output);
					Action.NotifyOnComplete(Untyped.init, &OnDiskReadComplete);					
				}
			}
		}
	}

	/// Gets the path on disk to store the cached contents for this file.
	string PathForCachedFile() {
		return SiteContaining.CacheDirectory ~ "\\" ~ this.Url ~ ".cache";
	}

	~this() {
		string DiskPath = PathForCachedFile();		
		if(exists(DiskPath))
			remove(DiskPath);
	}

	protected void OnDiskReadComplete(Untyped State, AsyncAction Action, CompletionType Type) {
		synchronized(this) {
			ReadPending = false;
			if(Type == CompletionType.Successful) {
				IOAction Act = cast(IOAction)Action;
				MemoryOutput Output = cast(MemoryOutput)Act.Output;
				_CachedContents = cast(string)Output.Data;
				_CachedLocations |= CacheStyle.Memory;								
			}
		}
	}
	
private:	
	string _CachedContents;
	Duration _CacheDuration;
	CacheStyle _CacheType;
	CacheStyle _CachedLocations;
	bool ReadPending;
	bool WritePending;	
}