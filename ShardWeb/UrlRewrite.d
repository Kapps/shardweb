﻿module ShardWeb.UrlRewrite;
private import ShardWeb.HttpRequest;
private import ShardTools.StringMap;
public import ShardTools.StringMatcher;


/// Rewrites a URL that matches a pattern to a different one.
abstract class UrlRewrite  {

public:
	/// Initializes a new instance of the UrlRewrite object.
	this(StringMatcher Pattern) {
		
	}

	/// The pattern required to be matched for this rewrite to take effect.
	StringMatcher Pattern() {
		return _Pattern;
	}

	/// Gets the rewritten url of the given request.
	/// Params:
	/// 	Request = The request to get the rewritten url for.
	abstract string GetRewrittenUrl(HttpRequest Request);
	
	
private:
	StringMatcher _Pattern;
}