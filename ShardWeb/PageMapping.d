﻿module ShardWeb.PageMapping;
import ShardWeb.Site;
import ShardWeb.Page;
import std.exception;
import ShardTools.ArrayOps;

/// Represents a mapping of a URL to an HttpRequestHandler.
class PageMapping  {

public:
	/// Initializes a new instance of the PageMapping object.
	/// Params:
	/// 	URL = The path to the page.
	/// 	CreatePageCallback = A callback that gets invoked when a new page is required to be created.
	this(string URL, Page delegate() CreatePageCallback, Site SiteContaining) {
		if(!URL.Contains('.'))
			URL ~= ".d";
		this._URL = URL;
		this._PageFactory = CreatePageCallback;
		this._SiteContaining = SiteContaining;
		enforce(SiteContaining !is null, "Unable to have a null Site container.");
	}

	/// Gets the URL this page is mapped to.
	@property string URL() {
		return _URL;
	}
	
	/// Gets a page for this thread.
	/// Params:
	///		ThreadID = The id of the thread to get a page for. This should be sequential (as in, if ThreadID 20 is passed in, threads 0 through 19 should exist as well).
	@property Page GetPageInstanceForThread(size_t ThreadID) {
		while(GeneratedPages.length <= ThreadID)
			GeneratedPages ~= _PageFactory();
		return GeneratedPages[ThreadID];
	}

	/// Gets the site containing this page.
	@property Site SiteContaining() {
		return _SiteContaining;
	}

private:
	string _URL;
	Page delegate() _PageFactory;
	Page[] GeneratedPages;
	Site _SiteContaining;
}