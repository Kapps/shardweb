﻿module ShardWeb.HttpResponse;
private import ShardTools.ArrayOps;
private import ShardTools.BufferPool;
private import ShardTools.Buffer;
private import ShardTools.StringTools;
private import std.datetime;
import ShardWeb.Page;
public import ShardNet.IRequest;
public import ShardNet.IResponse;
public import ShardWeb.HttpRequest;
import std.exception;
import std.string;
 
/// Represents a response to an HttpRequest.
class HttpResponse : IResponse {

public:
	/// Initializes a new instance of the HttpResponse object.
	///	Params:
	/// 	Request = The request that caused this response.
	this(HttpRequest Request) {
		this._Request = Request;
		this.ResponseValue = 200;		
		this._Encoding = EncodingType.Identity;
		this._Headers = new StringMap();
		this._Body = BufferPool.Global.Acquire(32767);
		this._SupportsChunked = Request.Parameters.Get("Version", "1.0") == "1.1";
		// Set _SupportsChunked. First, check HTTP version. Then, check things like if they allow it.
	}

	/// Gets the request that caused this response.
	@property HttpRequest Request() {
		return _Request;
	}

	/// Redirects the user to the given page, provided they have redirections enabled.
	/// Params:
	/// 	Page = The page to redirect the user to.
	final void RedirectTo(Page Page) {
		RedirectTo(Page.Url);
	}

	/// Redirects the user to the given page, provided they have redirections enabled.
	/// Params:
	/// 	Url = The Url to redirect the user to.
	final void RedirectTo(string Url) {
		this.ResponseValue = 301;
		AddHeader("Location: ", Url);
	}

	/// Gets or sets the status code for the HttpResponse. The default value is 200 (success).
	/// Params:
	/// 	StatusCode = The HTTP status code to set.
	@property void StatusCode(uint StatusCode) {
		this.ResponseValue = StatusCode;
	}

	/// Ditto
	@property override uint StatusCode() const {
		return ResponseValue;
	}
		
	/// Gets the headers for the response.
	@property StringMap Headers() {
		return _Headers;
	}

	/// Gets the data for this repsonse.
	/// It is possible for the response to resize, edit, or delete, the content data, and thus this should never be stored.
	/// If this response uses a chunked transfer type, this can either be null, or the first chunk (implementation defined).
	/// It is invalid to access this if the response is complete.
	@property const(ubyte[]) Content() const {
		return cast(const)(_Body.Data);
	}

	/// This is a temporary hack and should not be used. It will be removed soon.
	void SetBody(string Data) {
		enforce(!_IsReleased);
		_Body.Reuse(false);
		_Body.Write(Data);
	}

	/// Adds a header and assigns it the given value. If a header with that name already exists, it is overwritten.
	/// Params:
	/// 	HeaderName = The name of the header to add.
	/// 	Value = The value to assign to this header.
	void AddHeader(in char[] HeaderName, in char[] Value) {
		enforce(!_IsReleased, "Unable to write to a released response.");		
		// TODO: Possibly block more characters.
		// TODO: Possibly try decoding first. Are there any characters that can be decoded to \n that browsers would use?
		if(Contains(Value, '\n') || Contains(HeaderName, '\n'))
			throw new HttpException("HTTP Response Splitting exploit detected.");		
		// TODO: Remove the idup for Value. The cast is fine for Key because it's only used for its hash code.
		this._Headers.Set(cast(string)HeaderName, Value.idup);
		// TODO: Check if chunked encoding is being used and some data already sent, and if so, add this as a footer.
	}

	/// Gets the Buffer that AppendBody writes to.
	/// Note that AppendBody can perform some optimizations including replacing the buffer if faster than writing, and chunked encoding when enough bytes are written.
	/// Accessing this OutBuffer removes both of these optimizations.
	/// It is the callers responsibility to ensure that this Buffer is used in a threadsafe manner.
	@property Buffer OutBuffer() {
		if(!_ManuallyWriting) { // Don't force a lock when not needed.
			synchronized(this) {
				_ManuallyWriting = true; // Make sure AppendBody doesn't create a new buffer if something is accessing OutBuffer.
			}
		}
		return _Body;
	}

	/// Appends the given text to the body.
	/// Params:
	/// 	Text = The text to append to the body.
	void AppendBody(in char[] Text) {	
		synchronized(this) {	
			enforce(!_IsReleased, "Unable to write to a released response.");
			if(Text.length > 2048 && _Body.Count == 0 && !_ManuallyWriting) {
				// Instead of appending here, we just replace because it's faster.
				ubyte[] NonConst = cast(ubyte[])Text; // Not const but safe.
				assert(!_NoRelease);				
				BufferPool.Global.Release(_Body);
				_Body = Buffer.FromExistingData(NonConst);			
				_NoRelease = true;
			} else
				this._Body.Write(Text);
			if(_SupportsChunked && !_ManuallyWriting) {
				// TODO: Check if previous call to AppendBody.
			}
		}
		// TODO: After a certain amount of text is added, send using chunked encoding.
		// This would probably be ~8b.
		// Remember then to use chunked encoding, and check if HTTP version is 1.1!
		// Also, only do it if a second AppendBody is made; otherwise, we must assume a whole response is being written and chunked would be a waste.
	}

	/// Notifies the client to add the given cookie.		
	/// Params:
	///		Name = The name of this cookie.
	/// 	Value = The value to assign for this cookie.
	/// 	Domain = The domain that this cookie is valid on, or null if no domain.
	/// 	Expires = The date that this cookie expires, or min to set a session cookie. Note that this time is expected to be in GMT.
	/// 	Flags = The flags to assign for this cookie.
	void AddCookie(string Name, string Value, string Domain = null, DateTime Expires = DateTime.min, CookieFlags Flags = CookieFlags.None) {
		Cookie c = Cookie(Name, Value, Domain, Expires, Flags, CookieSender.Server);		
		_Cookies ~= c;		
	}

	/// Gets the cookies that have been set to be sent at the time of this call.
	@property CookieCollection CookiesToSend() const {
		return CookieCollection(cast(immutable)_Cookies);
	}
	
	/// Gets or sets the output encoding of this response.
	@property EncodingType ContentEncoding() const {
		return _Encoding;
	}

	// TODO: Remove this.
	/// Ditto
	@property void ContentEncoding(EncodingType Encoding) {
		_Encoding = Encoding;
	}

	/// Notifies this instance that the response has been fully sent, and is capable of releasing any resources used.
	void NotifyComplete() {		
		if(!_NoRelease)
			BufferPool.Global.Release(_Body);
		_Body = null;
		_IsReleased = true;
	}	

	/// Pauses generating this response until the 'fun' evaluates to true.
	/// This allows other responses to be processed while waiting for, say, a database request.
	@disable void YieldUntil(alias fun)() {
		// TODO: Something fiber related here?
		// Will need to add to PausedRequests or something, or just requeue it at top priority.
		// TODO: Static pages need to yield while reading from disk.
	}
	
private:
	StringMap _Headers;	
	Buffer _Body;
	HttpRequest _Request;
	int ResponseValue;
	EncodingType _Encoding;
	Cookie[] _Cookies;
	bool _SupportsChunked;
	bool _IsReleased;	
	/// If using a buffer from the caller of AppendBody, we don't want to release it because the input is const.
	bool _NoRelease;
	bool _ManuallyWriting;
} 