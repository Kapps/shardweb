﻿module ShardWeb.RequestProcessor;
public import ShardTools.StringMatcher;
public import ShardWeb.HttpResponse;
public import ShardWeb.Site;
public import ShardWeb.HttpRequest;


/// Allows viewing and manipulating a request before and after the request was handled.
/// Unlike pages, request processors are not created on a per-thread basis and instead the same instance passed in is used.
abstract class RequestProcessor {

public:	

	/// Initializes a new instance of the PatternMapping object.
	/// Params:
	/// 	Priority = Gets the priority of this mapping. A lower priority gets executed first.
	/// 	Pattern = Gets the StringMatcher used for this mapping.
	/// 	SiteContaining = Gets the Site that contains this preprocessor.
	this(int Order, StringMatcher Pattern, Site SiteContaining) {
		this._Order = Order;
		this._Pattern = Pattern;		
		this._SiteContaining = SiteContaining;
	}

	/// Gets the StringMatcher used for this mapping.
	@property final StringMatcher Pattern() {
		return _Pattern;
	}

	/// Gets the priority of this mapping. A lower order gets executed first.	
	@property final int Order() const {
		return _Order;
	}

	/// Gets the Site that contains this preprocessor.
	@property final Site SiteContaining() {
		return _SiteContaining;
	}

	/// Processes the given request prior to having a Page handle it.
	/// A state object may be returned for the postprocessing of the request.
	/// If the state object is not null, it gets passed into PostprocessRequest.
	abstract void* PreprocessRequest(HttpRequest Request);

	/// Processes a request response combo with the returned value of PreprocessRequest.
	/// If a request is canceled, this method will not be called.
	abstract void PostprocessRequest(HttpRequest Request, HttpResponse Response, void* State);
	
private:
	int _Order;
	Site _SiteContaining;
	StringMatcher _Pattern;
}