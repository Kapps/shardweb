﻿module ShardWeb.ApiProvider;
public import ShardWeb.StaticPage;

import ShardWeb.Page;

/// Provides a Page that contains one or more WebApis and generates JavaScript code to call them.
/// Each ApiPage is essentially a namespace to wrap the Apis it contains.
class ApiPage : StaticPage {

// TODO: This may wait for std.serialize, or some sort of nice JSON parsing.

public:
	/// Initializes a new instance of the ApiPage object.
	this(Site SiteContaining, string Namespace, string Directory = "/apis/") {
		this._Namespace = Namespace;
		this._Directory = Directory;
		string Uri = Directory ~ Namespace ~ ".js";
		// TODO: Add this to the site containing.
		super(SiteContaining, Uri, CacheStyle.Disk | CacheStyle.Memory);
	}

	/// Gets the namespace of this ApiPage.
	/// An Api is accessed and wrapped by the namespace.
	@property string Namespace() const {
		return _Namespace;
	}
	
	/// Gets the directory containing the script to generate.
	@property string Directory() const {
		return _Directory;
	}	
	
private:
	string _Namespace;	
	string _Directory;
}