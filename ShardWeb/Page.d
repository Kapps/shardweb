﻿module ShardWeb.Page;
public import ShardWeb.Authenticator;
public import ShardWeb.Site;
public import ShardWeb.HttpResponse;
public import ShardWeb.HttpRequest;

/// Represents a single Http Page.
/// For each worker thread, an instance of each Page is created.
/// Then when the page is accessed, the appropriate page for the given thread is used.
/// As such, the instance itself is not threaded, however multiple instances may be running concurrently.
abstract class Page  {

public: 
	/// Initializes a new instance of the Page object.
	this(string Uri, Site SiteContaining) {
		this._Url = Uri;
		this._SiteContaining = SiteContaining;
	}

	/// Handles a given Request, appending to the Response as output.	
	/// Params:
	/// 	Request = The request being handled.
	/// 	Response = The response used to handle the request.
	abstract void HandleRequest(HttpRequest Request, HttpResponse Response);

	/// Gets the Url required to navigate to this page. An example is '/test.d'.
	@property string Url() {
		return _Url;
	}

	/// Gets the site that this page is part of.
	@property Site SiteContaining() {
		return _SiteContaining;
	}
	
protected:
	
private:
	string _Url;
	Site _SiteContaining;	
}