﻿module ShardWeb.ErrorHandler;
private import ShardTools.StringMap;
import ShardWeb.HttpRequest;
import ShardTools.Logger;
import std.stdio;
import std.conv;
import std.socket;
struct ErrorInfo {
	Address RemoteIP;
	string Url;
	string Details;
	StringMap Parameters;
}

/// Used to handle errors. Currently sends a message with the info to the Logger class.
static class ErrorHandler  {

public:
	/// Notifies the ErrorHandler that an error has occurred.
	/// Params:
	/// 	Info = The information for the error.
	static void NotifyError(ErrorInfo Info, string FileName = __FILE__, int LineNumber = __LINE__) {
		string Message = "Error in " ~ Info.Url ~ " by " ~ to!string(Info.RemoteIP) ~ ". Details: " ~ Info.Details ~ ".";
		Message ~= " Parameters: " ~ to!string(Info.Parameters) ~ ". Occurred in " ~ FileName ~ "(" ~ to!string(LineNumber) ~ ").";
		writefln("%s", Message);
		Log("weberrors", Message);
		// TODO: More complex logging, probably with an event.
	}

	/// Notifies the ErrorHandler that an error has occurred.
	/// Params:
	///		Request = The request that caused the error.
	/// 	Details = The information for the error.
	static void NotifyError(HttpRequest Request, string Details, string FileName = __FILE__, int LineNumber = __LINE__) {
		ErrorInfo EI;
		EI.RemoteIP = Request.RemoteAddress;
		EI.Url = Request.UriRequested;
		EI.Details = Details;
		// IMPORTANT: TODO: Add this!
		//if(Request.Protocol != HttpRequestType.Post) 
			EI.Parameters = Request.Parameters;
		NotifyError(EI, FileName, LineNumber);
	}
	
private:
}