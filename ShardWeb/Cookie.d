﻿module ShardWeb.Cookie;
private import ShardTools.ExceptionTools;
private import std.uri;
private import std.exception;
import ShardTools.ArrayOps;
import std.datetime;

/// Sets any other flags needed for a cookie.
enum CookieFlags {
	None,
	HttpOnly,
	SecureOnly
}

/// Determines who sent a cookie, either the server or the browser.
enum CookieSender {
	Client,
	Server
}

mixin(MakeException("InvalidCookieException"));

/// Immutable struct representing a single cookie.
immutable struct Cookie  {

public immutable:
	/// The name of this cookie. The value is in encoded format if a Server cookie, or decoded if a Client cookie. As such, Server cookies should generally be write-only.
	string Name;
	/// The value assigned to this cookie. The value is in encoded format if a Server cookie, or decoded if a Client cookie. As such, Server cookies should generally be write-only.
	string Value;	
	/// The domain that this cookie is valid on. This parameter is unknown when Sender is Client. The value is in encoded format if a Server cookie, or decoded if a Client cookie. As such, Server cookies should generally be write-only.
	string Domain;
	/// Indicates when this cookie expires (GMT/UTC time), or DateTime.min for a session cookie. This parameter is unknown when Sender is Client.
	DateTime Expires;	
	/// The flags set for this cookie. This parameter is unknown when Sender is Client.
	CookieFlags Flags;
	/// The user that sent this cookie, either the client when informing the server about a cookie, or the server when setting a cookie.
	CookieSender Sender;
		
	this(string Name, string Value, string Domain, DateTime Expires, CookieFlags Flags, CookieSender Sender) {
		try {
			if(Sender == CookieSender.Client) {
				Name = decode(Name);
				Value = decode(Value);
				Domain = decode(Domain);
			} else if(Sender == CookieSender.Server) {
				Name = encode(Name);
				Value = encode(Value);
				Domain = encode(Domain);
			} else
				assert(0, "Unknown sender.");
		} catch (URIerror error) {
			// Catch and throw an exception instead.
			if(Sender == CookieSender.Client)
				throw new InvalidCookieException("Unable to decode a cookie from the client.");
			else
				throw new InvalidCookieException("Attempted to send a cookie that was unable to be encoded.");
		}
		this.Name = Name;
		this.Value = Value;				
		this.Domain = Domain;
		this.Expires = Expires;
		this.Flags = Flags;
		this.Sender = Sender;
	}
	
private:

}	