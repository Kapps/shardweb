﻿module ShardWeb.Authenticator;
public import ShardTools.StringMatcher;
public import ShardWeb.RequestProcessor;
private import ShardWeb.Page;
public import ShardWeb.HttpRequest;

/// Determines whether an HttpRequest has permission to access a given page.
abstract class Authenticator : RequestProcessor {

public:
	/// Initializes a new instance of the Authenticator object.	
	this(int Priority, StringMatcher Pattern, Site SiteContaining) {
		super(Priority, Pattern, SiteContaining);
	}

	/// Called when authentication fails. Usually performs a serverside redirect.
	abstract void OnAuthenticationFailed(HttpRequest Request);

	/// Indicates whether the given request should have access granted.
	abstract bool ShouldAllowAccess(HttpRequest Request);	

	/// Processes the given request prior to having a Page handle it.
	/// A state object may be returned for the postprocessing of the request.
	/// If the state object is not null, it gets passed into PostprocessRequest.
	override void* PreprocessRequest(HttpRequest Request) {
		if(!ShouldAllowAccess(Request))
			OnAuthenticationFailed(Request);
		return null;
	}

	/// Processes a request response combo with the returned value of PreprocessRequest.
	/// If a request is canceled, this method will not be called.
	override void PostprocessRequest(HttpRequest Request, HttpResponse Response, void* State) {
		
	}
	
private:

}