﻿module ShardWeb.FCGI.FCGIProperties;

/// Provides properties for a FastCGI Handler.
class FCGIProperties  {

public:
	/// Initializes a new instance of the FCGIProperties object.
	this() {
		
	}

	/// The port to listen on.
	public ushort Port = 0;

	/// Indicates whether to use FastCGI instead of standard CGI.
	public bool IsFastCGI = true;
	
private:
}