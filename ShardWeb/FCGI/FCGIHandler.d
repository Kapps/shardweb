﻿module ShardWeb.FCGI.FCGIHandler;
import ShardWeb.HttpResponse;
import std.algorithm;
import ShardNet.IResponse;
import std.array;
import ShardTools.Timer;
import core.thread;
import core.memory;
 
public import ShardWeb.HttpRequest;
public import ShardNet.IRequestHandler;
public import ShardWeb.FCGI.FCGIProperties;
public import ShardWeb.FCGI.FCGITypes;

import std.socket;
import std.socketstream;
import std.exception;
import ShardTools.List; 
import std.parallelism;
import std.conv;
import ShardTools.Logger;
import std.string : toLower, strip;
import std.datetime;
private import std.stdio;
private import ShardTools.StringTools;
import ShardTools.ArrayOps;

/// A handler class used to accept FastCGI connections, then dispatch the requests to a handler.
/// Multiple handlers are allowed, provided that they do not listen on the same port.
/// Each FCGIHandler instance can accept requests from any server. The dispatchers are unique, but the values are shared.
class FCGIHandler  {

public:
	/// Initializes the static instance of the FastCGI Handler.
	this(IRequestHandler Handler, FCGIProperties Properties) {
		ConnectedSockets = new List!(Socket)();
		this.Properties = Properties;
		this.Handler = Handler;
		this.socketSet = new SocketSet(128);
	}
	
	private void StartAccepting() {
		enforce(!IsListening, "May not make multiple calls to BeginAccept without calling EndAccept.");
		ListenSocket = new TcpSocket();						
		ushort Port = Properties.Port;
		ListenSocket.bind(new InternetAddress(Port));		
		ListenSocket.listen(8);
		ListenSocket.blocking = true;
		ConnectedSockets.Add(ListenSocket);
		IsListening = true;
		Thread LoopThread = new Thread(&RunNetworkLoop);
		LoopThread.start();
	}

	static ubyte[0xFFFF] Data; // Thread-local

	void HandleSocketReceive(Socket socket) {			
		ptrdiff_t NumBytes = socket.receive(Data);
		if(NumBytes == 0)
			return;		
		if(NumBytes == Socket.ERROR) 
			return;		
		/*immutable(ubyte[]) Data = cast(immutable(ubyte[]))"Content-type:text/html\r\n\r\n<html><head><title>Test</title></head><body><h1>!</h1><p>Testing!</p></body></html>";
			FCGIHeader OutputHeader = MakeHeader(cast(ushort)Data.length, FCGIRecordType.StdOut, 1);
			ubyte[] StdOutBuffer = new ubyte[OutputHeader.sizeof];
			*(cast(FCGIHeader*)StdOutBuffer.ptr) = OutputHeader;
			StdOutBuffer ~= Data;
			long BytesSent = cast(long)socket.send(StdOutBuffer);
			enforce(BytesSent != Socket.ERROR);		
			FCGIHeader EndRequestHeader = MakeHeader(FCGIEndRequestBody.sizeof, FCGIRecordType.EndRequest, 1);
			FCGIEndRequestBody EndBody;
			EndBody.AppStatus = 200;
			EndBody.ProtocolStatus = FCGIProtocolStatus.RequestComplete;
			ubyte[FCGIHeader.sizeof + FCGIEndRequestBody.sizeof] Output;		
			*(cast(FCGIHeader*)(Output.ptr + 0)) = EndRequestHeader;
			*(cast(FCGIEndRequestBody*)(Output.ptr + FCGIHeader.sizeof)) = EndBody;		
			size_t OutputBytesSent = socket.send(Output);
			enforce(OutputBytesSent != Socket.ERROR);						
			//if(CloseConnection || !Properties.IsFastCGI)
				//CloseSocket();								*/
		
		Timer timer = Timer.StartNew();				
		ushort RequestID = 0;		
		string ErrorDetails = "";
		try {						
			ubyte[] Input = Data[0..NumBytes];				
			//FCGIHeader Header = *(cast(FCGIHeader*)Input.ptr);				
			FCGIHeader Header = ReadHeader(Input);				
			RequestID = Header.RequestID;								
			if(Header.RecordType == FCGIRecordType.BeginRequest) {
				HandleBeginRequest(Input, socket, RequestID);		
			} else {
				ErrorDetails ~= "Unknown request type " ~ to!string(Header.RecordType) ~ "!";
				writefln("%s", ErrorDetails);								
			}
			//enforce(Input.length == 0, "Did not read all input received.");						
		} catch(Throwable Ex) {
			ErrorDetails ~= to!string(Ex);
			writefln("Exception!");
			writefln("%s", to!string(Ex));			
		}		
	}
	
	private void RunNetworkLoop() {		
		while(IsListening) {		
			socketSet.reset();
			socketSet.add(ListenSocket);		
			Socket[] sockets = ConnectedSockets.Elements.dup;				
			//foreach(Socket socket; ConnectedSockets)						
			for(int i = 0; i < sockets.length; i++) {
				if(sockets[i].isAlive())				
					socketSet.add(sockets[i]);
			}	
			int SocketCount;
			try {
				SocketCount = Socket.select(socketSet, null, null, 0);
			} catch (Throwable Ex) {
				writefln("%s", "Error selecting sockets. " ~ to!string(Ex) ~ ".");
				continue;
			}
			//foreach(Socket socket; ConnectedSockets) {			
			for(int i = 0; i < sockets.length; i++) {
				Socket socket = sockets[i];			
				bool IsSet = socketSet.isSet(socket) != 0;
				bool WasRemoved;
				if(!IsSet)
					continue;
				if(socket == ListenSocket) {
					Socket newSocket = ListenSocket.accept();	
					ConnectedSockets.Add(newSocket);		
				} else
					HandleSocketReceive(socket);						
			}			
			Thread.sleep(dur!"msecs"(1));
			// TODO: Make this go through all connected sockets and attempt to call receive on each of them.
			// Make sure non-blocking accept and receive. Use select for this?
			// Queue all requests, have X threads processing.
								
			//debug writefln("Received connection from " ~ to!string(newSocket.remoteAddress));
			/+if(newSocket is null) {
				Thread.sleep(10);
				continue;
			}+/
			
			//Thread LoopThread = new Thread(() { RunReceiveLoop(newSocket); });
			//LoopThread.start();
		}
	}



	private FCGIHeader ReadHeader(ref ubyte[] Input) {
		FCGIHeader Header;
		Header.Version = cast(FCGIVersions)Input[0];
		Header.RecordType = cast(FCGIRecordType)Input[1];
		Header.RequestID = (Input[2] << 8) | Input[3];
		Header.ContentLength = (Input[4] << 8) | Input[5];
		Header.PaddingLength = Input[6];
		Header.Reserved = Input[7];
		Input = Input[Header.sizeof..($ - Header.PaddingLength)];
		return Header;
	}

	private FCGIBeginRequestBody ReadBeginBody(ref ubyte[] Input) {
		FCGIBeginRequestBody Result;
		Result.Role = cast(FCGIRoles)((Input[0] << 8) | Input[1]);
		Result.Flags = cast(FCGIBeginRequestFlags)Input[2];
		Result.Reserved = Input[3..8];
		Input = Input[FCGIBeginRequestBody.sizeof..$];
		return Result;
	}

	private struct FCGIParameter {
		char[] Name;
		char[] Value;		
	}

	private static int ReadParamLength(ref ubyte[] Input) {
		uint Result;
		if(Input[0] >> 7 == 1) {
				Result = cast(int)((Input[0] & 0x7F) << 24) + (Input[1] << 16) + (Input[2] << 8) + (Input[3]);
				Input = Input[4..$];
		} else {
			Result = cast(int)Input[0];
			Input = Input[1..$];
		}			
		return Result;
	}

	private FCGIParameter[] ReadParameters(ref ubyte[] Input) {
		FCGIParameter[] Result = new FCGIParameter[0];
		while(Input.length > 0) {			
			uint NameLength = ReadParamLength(Input);
			uint ValueLength = ReadParamLength(Input);
			char[] Name = cast(char[])Input[0..NameLength];
			Input = Input[NameLength..$];
			char[] Value = cast(char[])Input[0..ValueLength];
			Input = Input[ValueLength..$];
			FCGIParameter Param;
			Param.Name = Name;
			Param.Value = Value;
			Result ~= Param;
		}
		return Result;
	}

	void GetParamMaps(FCGIParameter[] Params, out StringMap QueryMap, out StringMap ProtocolMap) {
		ProtocolMap = new StringMap;
		foreach(FCGIParameter Param; Params) {
			if(Param.Name is null || Param.Name.strip == "")
				continue;
			string ParamName = Param.Name.idup.toLower();		
			string ParamValue = Param.Value.idup;
			if(ParamValue is null || ParamValue.strip == "")
				ParamValue = "";			
			ProtocolMap.Set(ParamName, ParamValue);
		}		
		string QueryParams = ProtocolMap.Get!string("query_string", null);		
		string[] Split = QueryParams.split("&");
		QueryMap = new StringMap();
		foreach(string KVP; Split) {			
			size_t IndexEquals = KVP.IndexOf('=');			
			if(IndexEquals == -1 || IndexEquals == KVP.length - 1)
				QueryMap.Set(toLower(KVP), null);
			else
				QueryMap.Set(KVP[0..IndexEquals], KVP[IndexEquals + 1 .. $]);
		}		
	}

	private void HandleBeginRequest(ref ubyte[] Input, Socket socket, ushort RequestID) {
		/+string TestOutput = "<html><body><h2>Testing</h2></body></html>";
		FCGIHeader OutputHeader = MakeHeader(cast(ushort)TestOutput.length, FCGIRecordType.StdOut, 1);
		ubyte[] OutputBuffer = new ubyte[TestOutput.length + FCGIHeader.sizeof];
		for(int i = 0; i < TestOutput.length; i++)
			OutputBuffer[i] = cast(char)TestOutput[i];
		*(cast(FCGIHeader*)&OutputBuffer[TestOutput.length]) = OutputHeader;
		socket.send(OutputBuffer);
		return 200;+/
		void CloseSocket() {
			synchronized(ConnectedSockets) {
				socket.shutdown(SocketShutdown.BOTH);
				socket.close();				
				ConnectedSockets.Remove(socket);
			}
			//GC.collect();					
		}	
		Timer timer = Timer.StartNew();
		int StatusCode = 0;
		FCGIBeginRequestBody Body = ReadBeginBody(Input);
		bool CloseConnection = (Body.Flags & FCGIBeginRequestFlags.KeepConnection) == 0;		
		FCGIParameter[] Params = new FCGIParameter[0];
		FCGIHeader CurrentHeader = ReadHeader(Input);				
		Input = Input[0..$-CurrentHeader.PaddingLength];
		while(CurrentHeader.RecordType == FCGIRecordType.Params) {										
			if(CurrentHeader.ContentLength != 0) {
				ubyte[] InputPart = Input[0 .. CurrentHeader.ContentLength];			
				Input = Input[InputPart.length..$];
				FCGIParameter[] AddedParams = ReadParameters(InputPart);
				Params = Params ~ AddedParams;
			}			
			CurrentHeader = ReadHeader(Input);
		}							
		char[] StdIn = null;
		if(CurrentHeader.RecordType == FCGIRecordType.StdIn)
			StdIn = cast(char[])Input;
		StringMap QueryMap, ProtocolMap;
		GetParamMaps(Params, QueryMap, ProtocolMap);		
		// TODO: Temporary hack until I feel like parsing Address manually! Same for cookies... this stuff is unmaintained and old.
		HttpRequest Request = new HttpRequest(ProtocolMap.Get!string("script_name"), QueryMap, ProtocolMap, socket.remoteAddress(), null, RequestType.Dynamic);						
		Handler.QueueRequest(Request, (IResponse Response) {		
			//uint CurrRequest = RequestCount++;
			//IResponse Response = Handler.ProcessRequest(Request);
			HttpResponse AsHttpResponse = cast(HttpResponse)Response;
			if(AsHttpResponse !is null && AsHttpResponse.StatusCode != 200 && AsHttpResponse.StatusCode != 0)
				AsHttpResponse.AddHeader("Status", to!string(AsHttpResponse.StatusCode));
			immutable(ubyte[]) Data = cast(immutable)(cast(immutable(ubyte[]))ExtractHeaderString(AsHttpResponse) ~ cast(immutable(ubyte[]))"\r\n" ~ Response.Content);		
			FCGIHeader OutputHeader = MakeHeader(cast(ushort)Data.length, FCGIRecordType.StdOut, CurrentHeader.RequestID);
			ubyte[] StdOutBuffer = new ubyte[OutputHeader.sizeof];
			*(cast(FCGIHeader*)StdOutBuffer.ptr) = OutputHeader;
			StdOutBuffer ~= Data;
			long BytesSent = cast(long)socket.send(StdOutBuffer);
			enforce(BytesSent != Socket.ERROR);		
			FCGIHeader EndRequestHeader = MakeHeader(FCGIEndRequestBody.sizeof, FCGIRecordType.EndRequest, RequestID);
			FCGIEndRequestBody EndBody;
			EndBody.AppStatus = StatusCode;
			EndBody.ProtocolStatus = FCGIProtocolStatus.RequestComplete;
			ubyte[FCGIHeader.sizeof + FCGIEndRequestBody.sizeof] Output;		
			*(cast(FCGIHeader*)(Output.ptr + 0)) = EndRequestHeader;
			*(cast(FCGIEndRequestBody*)(Output.ptr + FCGIHeader.sizeof)) = EndBody;		
			size_t OutputBytesSent = socket.send(Output);
			enforce(OutputBytesSent != Socket.ERROR);						
			if(CloseConnection || !Properties.IsFastCGI)
				CloseSocket();								
			writefln("%s", "Responding to request number " ~ to!string(RequestCount++) ~ " took " ~ to!string(timer.Stop().Milliseconds) ~ " milliseconds. Status Code: " ~ to!string(StatusCode) ~ ".");			
		});			
	}

	private string ExtractHeaderString(HttpResponse Response) {
		string Result;
		foreach(string Key, Value; Response.Headers)
			Result ~= Key ~ ": " ~ Value ~ "\r\n";
		return Result;
	}


	private FCGIHeader MakeHeader(ushort ContentLength, FCGIRecordType RecordType, ushort RequestID) {
		FCGIHeader Result;		
		ubyte[2] ContentLengthBytes;
		ContentLengthBytes[0] = cast(ubyte)(ContentLength >> 8);
		ContentLengthBytes[1] = cast(ubyte)(ContentLength & 0xFF);
		Result.ContentLength = cast(ushort)(ContentLengthBytes[1] << 8) | ContentLengthBytes[0];	
		ushort ConvertedBack = (ContentLengthBytes[0] << 8) | ContentLengthBytes[1];
		if(ConvertedBack != ContentLength)
			writefln("%s", "Expected " ~ to!string(ContentLength) ~ " - Got: " ~ to!string(ConvertedBack) ~ ".");
		Result.PaddingLength = 0;
		Result.RecordType = RecordType;
		Result.RequestID = RequestID;
		Result.Reserved = 0;
		Result.Version = FCGIVersions.V1;
		return Result;
	}

	/// Begins accepting FastCGI connections, using the given RequestHandler to process them.
	///	Params:
	/// 	Handler = The handler to dispatch incoming requests to.
	/// 	Properties = The properties to use for the handler.
	public void BeginAccept() {
		StartAccepting(); 
	}	

	/// Stops accepting FastCGI connections, closing all active connections.
	public void EndAccept() {
		ListenSocket.shutdown(SocketShutdown.BOTH);
		ListenSocket.close();		
	}
	
private:	
	uint RequestCount;
	bool IsListening;	
	Socket ListenSocket;
	List!(Socket) ConnectedSockets;	
	IRequestHandler Handler;
	FCGIProperties Properties;
	SocketSet socketSet;
} 