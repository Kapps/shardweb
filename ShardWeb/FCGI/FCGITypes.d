﻿module ShardWeb.FCGI.FCGITypes;

enum int FCGI_LISTENSOCK_FILENO = 0;

/// Determines the type of a record.
enum FCGIRecordType : ubyte {
	None = 0,
	BeginRequest = 1,
	AbortRequest = 2,
	EndRequest = 3,
	Params = 4,
	StdIn = 5,
	StdOut = 6,
	StdErr = 7,
	Data = 8,
	GetValues = 9,
	GetValuesResult = 10,
	UnknownType = 11	
}

/// Determines the version of the FastCGI implementation.
enum FCGIVersions : ubyte {
	V1 = 1
}

/// Flags for a BeginRequest message.
enum FCGIBeginRequestFlags : ubyte {
	None = 0,
	KeepConnection = 1
}

/// The roles for a FastCGI application.
enum FCGIRoles : ushort {
	None = 0,
	Responder = 1,
	Authorizer = 2,
	Filter = 3
}

/// Determines the status of an EndRequest message.
enum FCGIProtocolStatus : ubyte {
	RequestComplete = 0,
	CantMultiplexConnections = 1,
	Overloaded = 2,
	UnknownRole = 3
}

/// Determines the value to get when requested.
enum  : string {
	FCGIMaxConnections = "FCGI_MAX_CONNS",
	FCGIMaxRequests = "FCGI_MAX_REQS",
	FCGIMultiplexConnections = "FCGI_MPXS_CONNS"
}

/// Represents a request containing the parameters for the beginning of a request.
struct FCGIBeginRequestBody {
	FCGIRoles Role;
	FCGIBeginRequestFlags Flags;
	ubyte[5] Reserved;
}

/// The standard message header for FastCGI messages.
struct FCGIHeader {
	FCGIVersions Version;
	FCGIRecordType RecordType;	
	ushort RequestID;	
	ushort ContentLength;
	ubyte PaddingLength;
	ubyte Reserved;
}

/// A record handling the beginning of a request.
struct FCGIBeginRequestRecord {
	FCGIHeader Header;
	FCGIBeginRequestBody Body;
}

/// The body for an UnknownType request.
struct FCGIUnknownTypeBody {
	ubyte Type;
	ubyte[7] Reserved;
}

/// A record for an unknown type request.
struct FCGIUnkownTypeRecord {
	FCGIHeader Header;
	FCGIUnknownTypeBody Body;
}

/// The body of an end request.
struct FCGIEndRequestBody {
	uint AppStatus;
	FCGIProtocolStatus ProtocolStatus;
	ubyte[3] Reserved;
}